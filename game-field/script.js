class GameField {
  // Информация о клетках поля
  state = [null, null, null, null, null, null, null, null, null];

  winnerCombinations = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  // Крестик или Нолик. Хранит активного игрока. Первые крестики ходят
  mode = 'x';
  // Состояние. Игра закончена или нет;
  isOverGame = false;
  // Состояние. Кто победил / Ничья;
  statusStateGame = 'Неясно';

  getGameFieldStatus() {
    // Победили Крестики Или победили Нолики
    console.log(this.statusStateGame, this.state);
  }
  // Метод, для изменения активного игрока
  setMode(currentMode) {
    currentMode === 'x' ? (this.mode = 'o') : (this.mode = 'x');
  }

  // Занята ли та ячейка в которую мы хотим поставить символ ?
  _hasAvailableState(event) {
    return event.target.localName !== 'img' ? true : false;
  }

  setFieldCellValue(event) {
    // console.log(event, event.target.innerText, typeof event.target.innerText);
    if (!this._hasAvailableState(event)) {
      alert('Занято!!! Давай поновой');
      return false;
    }
    const gridID = parseInt(event.target.id);
    console.log(`gridID -> ${gridID}, event.target.id -> ${event.target.id}`);

    if (this.mode === 'x') {
      this.state[gridID] = this.mode;
      gameStepField.innerHTML = 'Ходит&nbsp<img src="../images/zero.svg" />&nbsp;Владелен Пупкин';
      return (event.target.innerHTML = '<img src="../images/xxl-x.svg" />');
    } else {
      this.state[gridID] = this.mode;
      gameStepField.innerHTML = 'Ходит&nbsp<img src="../images/x.svg" />&nbsp;Владелен Пупкин';
      return (event.target.innerHTML = '<img src="../images/xxl-zero.svg" />');
    }

    // if (this.mode === 'x') {
    //   event.target.innerText = '<img src="../images/xxl-x.svg" />';
    // } else {
    //   event.target.innerText = '<img src="../images/xxl-zero.svg" />';
    // }
    // return (this.state[row][column] = this.mode);
  }

  // Проверка есть ли Нулевое значение. Если есть то игра незакончена
  _hasNullValue(squares) {
    for (let cell of squares) {
      if (cell === null) return true;
    }
    return false;
  }

  // Вычислить победителя в игре
  calculateWinners() {
    const currentGameField = this.state;
    const lines = this.winnerCombinations;
    const squares = [...currentGameField];
    console.log('squares ->', squares);

    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        this.statusStateGame = 'Победили -> ' + squares[a];
        return this.statusStateGame;
      }
    }

    if (!this._hasNullValue(squares)) {
      this.statusStateGame = 'Ничья';
      return this.statusStateGame;
    }

    return null;
  }
}

// ==================================
// ============== Main ==============
// ==================================
let getCells = document.querySelectorAll('.cell');
let gameStepField = document.querySelector('#game-step');
gameStepField.innerHTML = 'Ходит&nbsp<img src="../images/x.svg" />&nbsp;Владелен Пупкин';
console.log('gameStepField', gameStepField);
let gameField2 = new GameField();
if (!gameField2.isOverGame)
  for (let i = 0; i < getCells.length; i++) {
    getCells[i].addEventListener('click', (e) => gameLogic(e, gameField2, gameStepField));
    getCells[i].setAttribute('id', i);
  }

for (let i = 0; i < getCells.length; i++) {
  getCells[i].removeEventListener('click', (e) => gameLogic(e, gameField2, gameStepField));
  getCells[i].setAttribute('id', i + 7);
}

console.log(`gameField2.isOverGame ${gameField2.isOverGame}`);
console.log('end');
// gameField.getGameFieldStatus();

function gameLogic(event, gameField) {
  console.log(`cell click`, event);
  console.log('gameField', gameField);

  if (!gameField.setFieldCellValue(event)) return;
  gameField.setMode(gameField.mode);

  let resultGame = gameField.calculateWinners();
  if (resultGame) {
    gameField.isOverGame = true;
    console.log(`gameField---.isOverGame ${gameField.isOverGame}`);
    if (gameField.isOverGame) {
      for (let i = 0; i < getCells.length; i++) {
        getCells[i].removeEventListener('click', (e) => gameLogic(e, gameField, gameStepField));
      }
    }
    setTimeout(() => alert(`Game OVER! ${gameField.statusStateGame}`));

    return;
  }
}
// event.target.innerText = '<img src="../images/xxl-x.svg" />';

// let gameField = new GameField();
// console.log(gameField.mode);

// while (!gameField.isOverGame) {
//   if (!gameField.setFieldCellValue()) continue;

//   gameField.setMode(gameField.mode);
//   if (gameField.calculateWinners()) {
//     gameField.isOverGame = true;
//     break;
//   }
// }
// // Вывод сообщения кто победил.
// gameField.getGameFieldStatus();
