const loginInput = document.querySelector('input[name="login"]');
const passwordInput = document.querySelector('input[name="password"]');
const authorizationForm = document.querySelector('#authorization-form');
const MIN_PASS_LENGTH = 4;

let submit = document.querySelector('input[type="submit"]');

// Запрет на копирование
getDissableEventOnPaste(loginInput);
getDissableEventOnPaste(passwordInput);

// добавлинеие Слушателей событий
authorizationForm.addEventListener('submit', (event) => {
  event.preventDefault();
  console.log(`Login: ${loginInput.value}, password: ${passwordInput.value}`);
});

loginInput.addEventListener('keypress', (event) => {
  loginInput.classList.remove('wrongInputColor');
  if (!keypressHandler(loginInput.value + event.key)) {
    event.preventDefault();
    // loginInput.style.border = '1px solid red';
    loginInput.classList.add('wrongInputColor');
  }
});

passwordInput.addEventListener('keypress', (event) => {
  let fullInputValue = passwordInput.value + event.key;
  console.log(fullInputValue);
  passwordInput.classList.remove('wrongInputColor');
  hasMinLengthField(fullInputValue)
    ? submit.classList.add('active')
    : submit.classList.remove('active');
  if (!keypressHandler(fullInputValue)) {
    event.preventDefault();
    passwordInput.classList.add('wrongInputColor');
  }
});

// функция запрета на копирование.
function getDissableEventOnPaste(input) {
  input.onpaste = (e) => e.preventDefault();
}

function hasMinLengthField(fieldValue) {
  if (fieldValue.length >= 4) return true;
  return false;
}

function keypressHandler(input) {
  const allowedChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._';
  for (let i = 0; i < input.length; i++) {
    if (allowedChars.indexOf(input.charAt(i)) === -1) {
      return false;
    }
  }
  return true;
}
